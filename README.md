<h1 align="center">Coder_everyday</h1>
<h2 align="center">Whats up Nerds</h2>


- 🌱 I’m currently learning **Cyber Security,Programming,Networking**
- 💬 Ask me about **Ask me anything no problem ;)**
- ⚡ Fun fact: ***Follow me on  my social media account***


<h3 align="left">Follow me:</h3>
<p align="left">
<a href="instagram.com/coder_everyday/" target="blank"><img align="center" src="https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/instagram.svg" alt="coder" height="30" width="40" /></a>
<a href="https://www.youtube.com/channel/UCj4vvmy__YqulQJSF_qgu5A" target="blank"><img align="center" src="https://www.vectorlogo.zone/logos/youtube/youtube-icon.svg" alt="coder" height="40" width="40" /></a>
<a href="https://beacons.page/coder_everyday" target="blank"><img align="center" src="https://svgshare.com/i/WEc.svg" alt="coder" height="30" width="40" /></a>



<h3 align="left">Languages and Tools:</h3>
<p align="left">
<a href="https://www.gnu.org/software/bash/" target="_blank"> <img src="https://www.vectorlogo.zone/logos/gnu_bash/gnu_bash-icon.svg" alt="bash" width="40" height="40"/> </a> 
<a href="https://www.cprogramming.com/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/c/c-original.svg" alt="c" width="40" height="40"/> </a>
<a href="https://www.w3schools.com/cpp/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/cplusplus/cplusplus-original.svg" alt="cplusplus" width="40" height="40"/> </a>
<a href="https://git-scm.com/" target="_blank"> <img src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" alt="git" width="40" height="40"/> </a> 
<a href="https://www.w3.org/html/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original-wordmark.svg" alt="html5" width="40" height="40"/> </a> 
<a href="https://www.linux.org/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/linux/linux-original.svg" alt="linux" width="40" height="40"/> </a> 
<a href="https://www.python.org" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg" alt="python" width="40 "height="40"/> </a>
  <a href="https://golang.org" target="_blank"> <img src="https://www.vectorlogo.zone/logos/golang/golang-vertical.svg" alt="golang" width="40 "height="40"/> </a>
  </p>



<p><img align="left" src="https://github-readme-stats.vercel.app/api/top-langs?username=codereveryday&show_icons=true&theme=dark&hide_border=true&locale=en&layout=compact" alt="codereveryday" /></p>
<p><img align="center" src="https://github-readme-stats.vercel.app/api?username=codereveryday&show_icons=true&theme=dark&hide_border=true" alt="codereveryday" /></p>





<details>
<summary>:zap: Recent Project </summary>

<!--START_SECTION:activity-->
1. Configs [#Configs](https://github.com/codereveryday/Configs)
2. TDTF [#TDTF](https://github.com/codereveryday/The-Docker-Tool-Framework-)
3. Programming/Hacking Resources [#PHR](https://github.com/codereveryday/Programming-Hacking-Resources)
<!--END_SECTION:activity-->

</details>
